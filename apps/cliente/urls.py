from django.urls import path

from . import views

urlpatterns = [
    path('', views.principal, name= 'cliente'),
    path('crear_cliente/', views.crear),
    path('modificar_cliente/', views.modificar),
    path('iniciar_sesion/', views.iniciarSesion),
	path('pagina_principal/', views.paginaPrincipal),
    path('ver_cuenta/', views.verCuenta),
    path('eliminar_cliente/', views.eliminar),
    path('crear_cuenta/', views.crearCuenta),
    path(r'deposito/(?P<numero>d+)/$', views.depositar, name = 'deposito'),
    path(r'retiro/(?P<numero>d+)/$', views.retiro, name = 'retiro'),
]