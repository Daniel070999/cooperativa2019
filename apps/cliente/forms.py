from django import forms	
from apps.modelo.models import Cliente, Cuenta, Transacciones

class FormularioCliente(forms.ModelForm):
	class Meta:
		model = Cliente
		fields = ["cedula", "nombres", "apellidos", "fechaNacimiento", "genero", "estaCivil", "telefono",
					"celular", "direccion", "correo"] 
class FormularioModificarCliente(forms.ModelForm):
	class Meta:
		model = Cliente
		fields = [ "nombres", "apellidos", "fechaNacimiento", "genero", "estaCivil", "telefono",
					"celular", "direccion", "correo"] 
class FormularioCuenta(forms.ModelForm):
	class Meta: 
		model = Cuenta 
		fields = ["saldo",  "tipoCuenta"]
class FormularioTransaccion(forms.ModelForm):
	class Meta:
		model = Transacciones
		fields = ["valor", "descripcion"]