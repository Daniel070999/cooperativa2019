from django.shortcuts import render, redirect
import random
from django.contrib.auth.decorators import login_required
from .forms import FormularioCliente, FormularioModificarCliente, FormularioCuenta, FormularioTransaccion
from apps.modelo.models import Cliente, Cuenta, Transacciones
from django.db.models import Q



def principal(request):
	lista = Cliente.objects.all().order_by('apellidos')
	cuenta = Cuenta.objects.all()
	queryset = request.GET.get("buscar")
	print(queryset)
	if (queryset):
		lista = Cliente.objects.filter(
			Q(cedula__icontains = queryset) | #__icontains es como un like en mysql
			Q(nombres__icontains = queryset) |
			Q(celular__icontains = queryset)
		).distinct()
	context = {
		'lista' : lista,
		'cuenta' : cuenta,
	}
	return render (request, 'cliente/principal_cliente.html', context)

@login_required
def crear(request):
	num1 = (random.randrange(10))
	num2 = (random.randrange(10))
	num3 = (random.randrange(10)) 
	num4 = (random.randrange(10))
	num5 = (random.randrange(10))
	num6 = (random.randrange(10))
	num7 = (random.randrange(10))
	num8 = (random.randrange(10))
	num9 = (random.randrange(10))
	num10 = (random.randrange(10))
	nu1 = str(num1)
	nu2 = str(num2)
	nu3 = str(num3)
	nu4 = str(num4)
	nu5 = str(num5)
	nu6 = str(num6)
	nu7 = str(num7)
	nu8 = str(num8)
	nu9 = str(num9)
	nu10 = str(num10)

	num = nu1+nu2+nu3+nu4+nu5+nu6+nu7+nu8+nu9+nu10

	formulario = FormularioCliente(request.POST)
	formularioCuenta = FormularioCuenta(request.POST)
	usuario = request.user #la peticion que es procesada por el framework agrega el usuario
	if usuario.groups.filter(name = 'administrativo').exists():
		if request.method == 'POST':
			if formulario.is_valid() and formularioCuenta.is_valid():
				datos = formulario.cleaned_data #obteniendo todos los datos del formulario
				cliente = Cliente() #creo un objeto de la clase cliente
				cliente.cedula = datos.get('cedula')
				cliente.nombres = datos.get('nombres')
				cliente.apellidos = datos.get('apellidos')
				cliente.genero = datos.get('genero')
				cliente.fechaNacimiento = datos.get('fechaNacimiento')
				cliente.telefono = datos.get('telefono')
				cliente.celular = datos.get('celular')
				cliente.direccion = datos.get('direccion')
				cliente.estaCivil = datos.get('estaCivil')
				cliente.correo = datos.get('correo')
				cliente.save()
				datosCuenta = formularioCuenta.cleaned_data #obteniendo los datos del formulario cuenta
				cuenta = Cuenta() #creo un objeto de la clase cuenta
				cuenta.numero = num
				cuenta.estado = True
				cuenta.fechaApertura = datosCuenta.get('fechaApertura')
				cuenta.tipoCuenta = datosCuenta.get('tipoCuenta')
				cuenta.saldo = datosCuenta.get('saldo')
				cuenta.cliente = cliente
				cuenta.save()
				return redirect(principal)
	else:
		return render (request, 'cliente/acceso_prohibido.html')
	context = {
		'f': formulario,  
		'fc': formularioCuenta, 
	}
	return render (request, 'cliente/crear_cliente.html', context)

def crearCuenta(request):
	client = request.GET['cedula']
	clien = Cliente.objects.get(cedula = client)
	cli = clien.cliente_id
	formularioCuenta = FormularioCuenta(request.POST)
	num1 = (random.randrange(10))
	num2 = (random.randrange(10))
	num3 = (random.randrange(10)) 
	num4 = (random.randrange(10))
	num5 = (random.randrange(10))
	num6 = (random.randrange(10))
	num7 = (random.randrange(10))
	num8 = (random.randrange(10))
	num9 = (random.randrange(10))
	num10 = (random.randrange(10))
	nu1 = str(num1)
	nu2 = str(num2)
	nu3 = str(num3)
	nu4 = str(num4)
	nu5 = str(num5)
	nu6 = str(num6)
	nu7 = str(num7)
	nu8 = str(num8)
	nu9 = str(num9)
	nu10 = str(num10)

	num = nu1+nu2+nu3+nu4+nu5+nu6+nu7+nu8+nu9+nu10
	if request.method == 'POST':
		if  formularioCuenta.is_valid():
			cliente = Cliente() #creo un objeto de la clase cliente
			datosCuenta = formularioCuenta.cleaned_data #obteniendo los datos del formulario cuenta
			cuenta = Cuenta() #creo un objeto de la clase cuenta
			cuenta.numero = num
			cuenta.estado = True
			cuenta.fechaApertura = datosCuenta.get('fechaApertura')
			cuenta.tipoCuenta = datosCuenta.get('tipoCuenta')
			cuenta.saldo = datosCuenta.get('saldo')
			cuenta.cliente_id = cli
			cuenta.save()
			return redirect(principal)
	context = {
		'fc': formularioCuenta, 
	}

	return render(request, 'cliente/crear_cuenta.html', context)

def verCuenta(request):
	cliente = request.GET['cedula']
	client = Cliente.objects.get(cedula = cliente)
	cuent = Cuenta.objects.filter(cliente_id = client.cliente_id)
	context = {
		'client' : client,
		'cuent' : cuent,
	}
	return render(request, 'cuenta/ver_cuenta.html', context)
def eliminar(request):
	cliente = request.GET['cedula']
	Cliente.objects.filter(cedula=cliente).delete()
	return render(request, 'cliente/eliminar_cliente.html')

def modificar(request):
	dni = request.GET['cedula']
	cliente = Cliente.objects.get(cedula = dni)


	if request.method == 'POST':
		formulario =FormularioModificarCliente(request.POST)
		if formulario.is_valid():
			datos = formulario.cleaned_data
			cliente.nombres = datos.get('nombres')
			cliente.apellidos = datos.get('apellidos')
			cliente.genero = datos.get('genero')
			cliente.fechaNacimiento = datos.get('fechaNacimiento')
			cliente.telefono = datos.get('telefono')
			cliente.celular = datos.get('celular')
			cliente.direccion = datos.get('direccion')
			cliente.estaCivil = datos.get('estaCivil')
			cliente.correo = datos.get('correo')
			cliente.save() 
			return redirect(principal)
	else:
		formulario = FormularioModificarCliente(instance = cliente)
	context= {
		'dni' : dni,
		'cliente' : cliente,
		'formulario' : formulario,
	} 
	return render (request, 'cliente/modificar_cliente.html', context)


def iniciarSesion(request):

	return render (request, 'login/iniciarSesion.html')

def paginaPrincipal(request):

	return render (request, 'login/pagina_principal.html')

def depositar (request, numero):
	cuenta = Cuenta.objects.get(numero = numero)
	cliente = Cliente.objects.get(cliente_id = cuenta.cliente_id)
	formulario = FormularioTransaccion(request.POST)
	if(request.method == 'POST'):
		if formulario.is_valid():
			datos = formulario.cleaned_data
			cuenta.saldo = cuenta.saldo + datos.get('valor')
			cuenta.save()
			transaccion = Transacciones()
			transaccion.tipo = 'deposito';
			transaccion.valor = datos.get('valor')
			transaccion.descripcion = datos.get('descripcion')
			transaccion.responsable = 'xxxxxxxxxx'
			transaccion.cuenta = cuenta
			transaccion.save()
			deposito = float(datos.get('valor'))
			mensaje = 'Transaccion exitosa'
			return render(request,'transaccion/estatus.html',locals())
	context = {
	'cliente' : cliente,
	'cuenta' : cuenta,
	'formulario' : formulario,
	}
	return render (request, 'transaccion/depositar.html', context)

def retiro (request, numero):
	cuenta = Cuenta.objects.get(numero = numero)
	cliente = Cliente.objects.get(cliente_id = cuenta.cliente_id)
	formulario = FormularioTransaccion(request.POST)
	if(request.method == 'POST'):
		if formulario.is_valid():
			datos = formulario.cleaned_data
			cuenta.saldo = cuenta.saldo - datos.get('valor')
			cuenta.save()
			transaccion = Transacciones()
			transaccion.tipo = 'deposito';
			transaccion.valor = datos.get('valor')
			transaccion.descripcion = datos.get('descripcion')
			transaccion.responsable = 'xxxxxxxxxx'
			transaccion.cuenta = cuenta
			transaccion.save()
			mensaje = 'Transaccion exitosa'
			return render(request,'transaccion/estatus.html',locals())
	context = {
	'cliente' : cliente,
	'cuenta' : cuenta,
	'formulario' : formulario,
	}
	return render (request, 'transaccion/retiro.html', context)













