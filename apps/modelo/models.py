from django.db import models
import random

class Cliente(models.Model):
	listaGenero = (
		('femenino','Femenino'),
		('masculino','Masculino'),
	)
	estadoCivil = (
		('casado','Casado'),
		('soltero','Soltero'),
		('divorciado','Divorciado'),
		('viudo','Viudo'),
	)
	cliente_id = models.AutoField(primary_key = True)
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
	direccion = models.TextField(max_length = 50 , default = 'sin direccion')
	telefono = models.CharField(max_length = 13)
	celular = models.CharField(max_length = 13) 
	genero = models.CharField(max_length = 15, choices = listaGenero , default = 'femenino', null = False)
	estaCivil = models.CharField(max_length = 50, choices = estadoCivil, null = False)
	fechaNacimiento = models.DateField(auto_now = False,auto_now_add = False, null = False)
	correo = models.EmailField(max_length = 50, null = False)
class Cuenta(models.Model):
	listaTipo = (
		('corriente', 'Corriente'),
		('ahorros', 'Ahorros'),
	)
	cuenta_id = models.AutoField(primary_key = True)
	numero = models.CharField(max_length = 10, unique = True, null = False)
	estado = models.BooleanField(default = True)
	fechaApertura = models.DateField(auto_now_add = True, null = False)
	tipoCuenta = models.CharField(max_length = 30, choices = listaTipo, null = False, default = 'ahorros')
	saldo = models.DecimalField(max_digits = 10, decimal_places = 3, null = False)
	cliente = models.ForeignKey(
		'Cliente',
		on_delete = models.CASCADE,
	)
	def __str__(self):
		string = str(self.saldo)+";"+str(self.cuenta_id)
		return string
class Transacciones(models.Model):
	listaTipoT = (
		('retiro','Retiro'),
		('deposito','Deposito'),
		('transferencia','Transaferencia'),
	)

	transaccion_id = models.AutoField(primary_key = True)
	fecha = models.DateTimeField(auto_now_add = True , null = False)
	tipo = models.CharField(max_length = 30, choices = listaTipoT, null = False)
	valor = models.DecimalField(max_digits = 10, decimal_places = 3, null = False)
	descripcion = models.TextField(null = False)
	responsable = models.CharField(max_length = 160, null = False)
	cuenta = models.ForeignKey(
		'Cuenta',
		on_delete = models.CASCADE,
	)