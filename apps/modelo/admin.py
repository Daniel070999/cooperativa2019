from django.contrib import admin
from .models import Cliente
from .models import Cuenta
from .models import Transacciones

class AdminCliente(admin.ModelAdmin):
	list_display = ["cedula", "apellidos", "nombres", "genero","celular"]
	list_editable = ["apellidos", "nombres"]
	list_filter = ["genero","estaCivil"]
	search_fields = ["cedula", "apellidos"]
	class Meta:
		model = Cliente
admin.site.register(Cliente, AdminCliente)


class AdminCuenta(admin.ModelAdmin):
	list_display = ["numero", "estado", "tipoCuenta","saldo"]
	list_editable = ["estado"]
	list_filter = ["numero", "tipoCuenta"]
	search_fields = ["numero", "estado"]
	class Meta:
		model = Cuenta
admin.site.register(Cuenta, AdminCuenta)


class AdminTransacciones(admin.ModelAdmin):
	list_display = ["fecha","tipo","valor","responsable"]
	list_editable = ["responsable"]
	list_filter = ["tipo","fecha"]
	search_fields = ["tipo", "responsable"]
	class Meta:
		model = Transacciones
admin.site.register(Transacciones, AdminTransacciones)